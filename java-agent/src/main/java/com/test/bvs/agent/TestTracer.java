package com.test.bvs.agent;

public class TestTracer {
    private static int count;
    public static void startTracer(Object object) {
        ++count;
        System.out.println("###### TestTracer::startTracer - method invoked [" + getCount() + "], object=" + object);
    }

    public static int getCount() {
        return count;
    }
}
