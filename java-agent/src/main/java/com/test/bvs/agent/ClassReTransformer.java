package com.test.bvs.agent;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.InsnList;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.VarInsnNode;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;

public class ClassReTransformer implements ClassFileTransformer {
    private String transformClassName;

    @Override
    public byte[] transform(ClassLoader loader, String className, Class<?> classBeingRedefined,
        ProtectionDomain protectionDomain, byte[] classfileBuffer) throws IllegalClassFormatException {
        if (!className.equals(transformClassName)) {
            return null;
        }

        return instrumentClass(classfileBuffer);
    }

    public void setTransformClassName(String transformClassName) {
        this.transformClassName = transformClassName;
    }

    private byte[] instrumentClass(byte[] classBytes) {
        ClassNode classNode = new ClassNode();
        ClassReader classReader = new ClassReader(classBytes);
        classReader.accept(classNode, 0);

        // Instrument all public non-static members
        for (MethodNode mn : classNode.methods) {
            if (!mn.name.equals("<init>") &&
                (mn.access & Opcodes.ACC_PUBLIC) == Opcodes.ACC_PUBLIC &&
                (mn.access & Opcodes.ACC_STATIC) == 0 &&
                (mn.access & Opcodes.ACC_NATIVE) == 0 &&
                (mn.access & Opcodes.ACC_ABSTRACT) == 0) {
                InsnList il = new InsnList();
                il.add(new VarInsnNode(Opcodes.ALOAD, 0));
                il.add(new MethodInsnNode(Opcodes.INVOKESTATIC, "com/test/bvs/agent/TestTracer",
                    "startTracer", "(Ljava/lang/Object;)V", false));
                AbstractInsnNode firstNode = mn.instructions.getFirst();
                if (firstNode != null) {
                    mn.instructions.insertBefore(mn.instructions.getFirst(), il);
                }
            }
        }

        ClassWriter classWriter = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
        classNode.accept(classWriter);
        return classWriter.toByteArray();
    }
}
