package com.test.bvs.agent;

import java.lang.instrument.Instrumentation;
import java.lang.instrument.UnmodifiableClassException;

public class JavaAgent {
    static private ClassReTransformer classReTransformer;
    static private Instrumentation instrumentation;

    public static void premain(String args, Instrumentation instrumentation) {
        try {
            JavaAgent.instrumentation = instrumentation;
            classReTransformer = new ClassReTransformer();
            instrumentation.addTransformer(classReTransformer, true);
            System.out.println("####### Java Agent ::: premain - Transformer added");
        } catch (Exception ex) {
            System.out.println("####### Java Agent ::: premain - Transformer error : " + ex.getMessage());
        }
    }

    public static void agentmain(String args, Instrumentation instrumentation) {
        try {
            JavaAgent.instrumentation = instrumentation;
            classReTransformer = new ClassReTransformer();
            instrumentation.addTransformer(classReTransformer, true);
            System.out.println("####### Java Agent ::: agentmain :: Transformer added");
        } catch (Exception ex) {
            System.out.println("####### Java Agent ::: agentmain - Transformer error : " + ex.getMessage());
        }
    }

    public static void transformClass(Class<?> clazz) throws UnmodifiableClassException {
        try {
            classReTransformer.setTransformClassName(clazz.getName().replace('.', '/'));
            instrumentation.retransformClasses(clazz);
            System.out.println("####### Java Agent ::: Transform class completed for " + clazz.getName());
        } catch (Exception ex) {
            System.out.println("####### Java Agent ::: transformClass - Transformer error : " + ex.getMessage());
        }
    }

}
