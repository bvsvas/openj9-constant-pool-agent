# openj9-constant-pool-agent

## IBM OpenJ9 VM Constant Pool Issue after Class Retransformation

IBM JVM throws error AnnotationFormatError `java.lang.IllegalArgumentException: Wrong type at constant pool index` after class re-transformation.

**Note:** This issue comes when you re-transform class with method parameters having annotation. E.g.: Rest Controller methods with ``@RequestBody`` annotation.

## Project Details
This project has two maven modules one for java agent and another one spring boot application.

- java-agent
- spring-tomcat-http-app - It is going to build war artifact hence deploy this war file in tomcat or any other webserver

### Prerequisite
- IBM J9 VM (JDK 8)
- Tomcat 9.x

**Note:** Must set JAVA_HOME to IBM JDK before running the application as this is reproducible in IBM J9 JVM. 

## Build and Deploy

- [ ] Clone the project `https://gitlab.com/bvsvas/openj9-constant-pool-agent.git`
- [ ] Build the project using Maven

Here is the instructions: 
```agsl
git clone https://gitlab.com/bvsvas/openj9-constant-pool-agent.git
cd openj9-constant-pool-agent
mvn clean install
```

#### Verify artifacts
- java-agent.jar in openj9-constant-pool-agent/java-agent/target
- spring-tomcat-http-app.war in openj9-constant-pool-agent/spring-tomcat-http-app/target

#### Download IBM JDK
- [ ] [IBM JDK Download page](https://developer.ibm.com/languages/java/semeru-runtimes/downloads/)
  - Choose Java 8(LTS), version 8.0.362.0 [IBM JDK 8 Windows link](https://github.com/ibmruntimes/semeru8-binaries/releases/download/jdk8u362-b09_openj9-0.36.0/ibm-semeru-open-jdk_x64_windows_8u362b09_openj9-0.36.0.zip)

#### Download Tomcat 9.x
Download and Install/Unzip tomcat 9.x version (apache-tomcat-9.0.74 or anything)
- [ ] [Tomcat 9.x Download page](https://tomcat.apache.org/download-90.cgi)

### Deploy war into the Tomcat
- [ ] Unzip the tomcat `apache-tomcat-9.0.74`
- [ ] Copy spring-tomcat-http-app.war (built in above step) into the webapps `apache-tomcat-9.0.74/webapps`
- [ ] Set the java-agent.jar file using JAVA_OPTS environment variable (alternatively edit in catalina.bat/catalina.sh)
- [ ] Start Tomcat (startup.bat or startup.sh). Observe Tomcat console logs - Spring Boot application should be initialized.

```py
# Set this value in JAVA_OPTS
-javaagent:<APP-FOLDER>/openj9-constant-pool-agent/java-agent/target/java-agent.jar
```

## Steps to reproduce the issue

- [ ] Transform the class `curl http://<host>:<port>/spring-tomcat-http-test/agent/transform`
  - This will transform the Rest Controller class all methods (here it uses `CarsController`) 
- [ ] Hit the Save Car REST API `curl --location --request POST 'http://<host>:<port>/honda/saveCar' --header 'Content-Type: application/json' --data '{"id": "12345", "model": "Honda", "productionYear": "2023", "color": "Green"}'`
  - This will throw `Wrong type at constant pool index` error, check Tomcat console logs.


## Error Stack trace

```
Caused by: java.lang.IllegalArgumentException: Wrong type at constant pool index
	at sun.reflect.ConstantPool.getClassAt0(Native Method)
	at sun.reflect.ConstantPool.getClassAt(ConstantPool.java:49)
	at sun.reflect.annotation.AnnotationParser.parseAnnotation2(AnnotationParser.java:256)
	at sun.reflect.annotation.AnnotationParser.parseAnnotation(AnnotationParser.java:238)
	at sun.reflect.annotation.AnnotationParser.parseParameterAnnotations2(AnnotationParser.java:196)
	at sun.reflect.annotation.AnnotationParser.parseParameterAnnotations(AnnotationParser.java:173)
Caused by: java.lang.annotation.AnnotationFormatError: java.lang.IllegalArgumentException: Wrong type at constant pool index
	at sun.reflect.annotation.AnnotationParser.parseParameterAnnotations(AnnotationParser.java:179)
	at java.lang.reflect.Executable.parseParameterAnnotations(Executable.java:91)
	at java.lang.reflect.Executable.sharedGetParameterAnnotations(Executable.java:566)
	at java.lang.reflect.Method.getParameterAnnotations(Method.java:660)
	at org.springframework.core.MethodParameter.getParameterAnnotations(MethodParameter.java:635)
	at org.springframework.web.method.HandlerMethod$HandlerMethodParameter.getParameterAnnotations(HandlerMethod.java:541)
	at org.springframework.core.MethodParameter.getParameterAnnotation(MethodParameter.java:668)
	at org.springframework.core.MethodParameter.hasParameterAnnotation(MethodParameter.java:683)
	at org.springframework.web.method.annotation.ModelFactory.findSessionAttributeArguments(ModelFactory.java:182)
	at org.springframework.web.method.annotation.ModelFactory.initModel(ModelFactory.java:114)
	at org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.invokeHandlerMethod(RequestMappingHandlerAdapter.java:872)
	at org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.handleInternal(RequestMappingHandlerAdapter.java:808)
	at org.springframework.web.servlet.mvc.method.AbstractHandlerMethodAdapter.handle(AbstractHandlerMethodAdapter.java:87)
	at org.springframework.web.servlet.DispatcherServlet.doDispatch(DispatcherServlet.java:1067)
```
