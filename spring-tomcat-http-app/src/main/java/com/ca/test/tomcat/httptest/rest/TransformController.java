package com.ca.test.tomcat.httptest.rest;

import com.ca.test.tomcat.httptest.transform.TransformUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/agent")
public class TransformController {

    private boolean isTransformCalled = false;

    @GetMapping(path = "/transform", produces = "application/json")
    public String transformClass() {
        if(isTransformCalled) {
            return "{\"status\":\"already transformed\"}";
        }

        if(TransformUtil.transformClass("com.ca.test.tomcat.httptest.rest.CarsController")) {
            isTransformCalled = true;
        }

        return "{\"status\":\"transformed\"}";
    }
}
