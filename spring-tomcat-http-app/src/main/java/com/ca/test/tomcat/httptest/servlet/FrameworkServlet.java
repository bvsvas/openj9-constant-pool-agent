package com.ca.test.tomcat.httptest.servlet;

import io.micrometer.core.annotation.Timed;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/hello")
public class FrameworkServlet extends HttpServlet {

    @Timed(value = "helloGetEndpoint.timer", description = "Time taken to process hello endpoint")
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Timed(value = "helloPostEndpoint.timer", description = "Time taken to process hello endpoint")
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.getWriter().println("Hello, World!");
    }
}
