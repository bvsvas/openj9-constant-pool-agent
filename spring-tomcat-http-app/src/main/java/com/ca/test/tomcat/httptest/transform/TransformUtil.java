package com.ca.test.tomcat.httptest.transform;

import com.test.bvs.agent.JavaAgent;
import lombok.extern.slf4j.Slf4j;

import java.lang.instrument.UnmodifiableClassException;

@Slf4j
public class TransformUtil {

    public static boolean transformClass(String className) {
        try {
            Class<?> clazz = TransformUtil.class.getClassLoader().loadClass(className);
            JavaAgent.transformClass(clazz);
            return true;
        } catch (ClassNotFoundException | UnmodifiableClassException ex) {
            log.info(ex.getMessage(), ex);
            return false;
        }
    }
}
