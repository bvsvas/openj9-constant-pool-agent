package com.ca.test.tomcat.httptest.servlet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;

import javax.annotation.PostConstruct;

@Configuration
public class AppWebConfig {

    @Autowired
    private RequestMappingHandlerAdapter requestMappingHandlerAdapter;

    @PostConstruct
    public void init() {
        System.out.println("$$$$ AppWebConfig - Init Method - requestMappingHandlerAdapter=" + requestMappingHandlerAdapter);
        requestMappingHandlerAdapter.setSynchronizeOnSession(true);
    }
}
