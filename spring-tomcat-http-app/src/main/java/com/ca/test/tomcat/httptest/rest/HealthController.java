package com.ca.test.tomcat.httptest.rest;

import io.micrometer.core.annotation.Timed;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/test")
public class HealthController {

    @Timed(value = "helloCar.timer", description = "Time taken to process helloCar endpoint")
    @PostMapping(path = "/helloCar", produces = "application/json")
    public String helloCar() {
        return "{\"car\":\"hello\"}";
    }

    @Timed(value = "healthzCar.timer", description = "Time taken to process healthzCar endpoint")
    @GetMapping(path = "/healthzCar", produces = "application/json")
    public String healthCar() {
        return "{\"car\":\"healthy\"}";
    }
}
