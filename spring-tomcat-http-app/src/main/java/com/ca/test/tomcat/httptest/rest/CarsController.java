package com.ca.test.tomcat.httptest.rest;

import com.ca.test.tomcat.httptest.model.Car;
import com.ca.test.tomcat.httptest.model.CarResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@RestController
@RequestMapping(path = "/honda")
public class CarsController {

    private final Map<String, Car> carsCache = new ConcurrentHashMap<>();

    @GetMapping(path = "/staticCars", produces = "application/json")
    public List<Car> getStaticCars() {
        List<Car> cars = new ArrayList<>();
        Car car;
        car = new Car();
        car.setCarDetail("1", "Ford", "2021", "Red");
        carsCache.put(car.getId(), car);
        cars.add(car);

        car = new Car();
        car.setCarDetail("2", "Tesla", "2022", "Blue");
        carsCache.put(car.getId(), car);
        cars.add(car);

        car = new Car();
        car.setCarDetail("3", "Chevrolet", "2021", "Yellow");
        carsCache.put(car.getId(), car);
        cars.add(car);

        return cars;
    }

    @PostMapping(path = "/saveCar", produces = "application/json")
    public ResponseEntity<CarResponse> saveCar(@RequestBody Car carRequest) {
        CarResponse carStatus = new CarResponse();
        carStatus.setId(carRequest.getId());

        if(carRequest == null) {
            carStatus.setStatus("Cars json is not valid");
            return new ResponseEntity<>(carStatus, HttpStatus.BAD_REQUEST);
        }

        if(carRequest.getId() == null || "".equals(carRequest.getId())) {
            carStatus.setStatus("Cars ID must not be empty");
            return new ResponseEntity<>(carStatus, HttpStatus.BAD_REQUEST);
        }

        try {
            Integer.parseInt(carRequest.getId());
        } catch (NumberFormatException nfe) {
            carStatus.setStatus("Cars ID must be an valid numerical value");
            return new ResponseEntity<>(carStatus, HttpStatus.BAD_REQUEST);
        }

        if("105".equals(carRequest.getId())) {
            try {
                Thread.sleep(20000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        carsCache.put(carRequest.getId(), carRequest);

        carStatus.setStatus("saved successfully");
        return new ResponseEntity<>(carStatus, HttpStatus.OK);
    }

    @GetMapping(path = "/getCars", produces = "application/json")
    public List<Car> getCars() {
        return new ArrayList<>(carsCache.values());
    }

    @GetMapping(path = "/getCar", produces = "application/json")
    public ResponseEntity<Car> getCar(@RequestParam String id) {
        if(id == null || id.isEmpty()) {
            Car car = new Car();
            car.setStatus("ID cannot be empty.");
            return new ResponseEntity<>(car, HttpStatus.BAD_REQUEST);
        }

        try {
            Integer.parseInt(id);
        } catch (NumberFormatException nfe) {
            Car car = new Car();
            car.setStatus("Cars ID must be an valid numerical value.");
            return new ResponseEntity<>(car, HttpStatus.BAD_REQUEST);
        }

        if("105".equals(id)) {
            try {
                Thread.sleep(20000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if(carsCache.containsKey(id)) {
            return new ResponseEntity<>(carsCache.get(id), HttpStatus.OK);
        } else {
            Car car = new Car();
            car.setStatus("ID not found.");
            return new ResponseEntity<>(car, HttpStatus.BAD_REQUEST);
        }
    }

    private void sleep() {
        try {
            Thread.sleep(50);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
