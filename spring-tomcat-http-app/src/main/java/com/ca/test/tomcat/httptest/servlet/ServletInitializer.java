package com.ca.test.tomcat.httptest.servlet;

import com.ca.test.tomcat.httptest.SpringEmployeeApp;
import com.ca.test.tomcat.httptest.config.MetricsConfiguration;
import com.ca.test.tomcat.httptest.rest.CarsController;
import com.ca.test.tomcat.httptest.rest.EmployeeController;
import com.ca.test.tomcat.httptest.rest.HealthController;
import com.ca.test.tomcat.httptest.rest.TransformController;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletContext;

public class ServletInitializer extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(SpringEmployeeApp.class, ServletConfig.class, AppWebConfig.class, MetricsConfiguration.class)
                .initializers(new RestControllerInitializer(new CarsController(),
                        new HealthController(),
                        new EmployeeController(),
                        new TransformController()));
    }

    @Override
    protected WebApplicationContext createRootApplicationContext(ServletContext servletContext) {
        return super.createRootApplicationContext(servletContext);
    }

    public static class RestControllerInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

        private final CarsController carsController;
        private final EmployeeController employeeController;

        private final HealthController healthController;

        private final TransformController transformController;

        public RestControllerInitializer(CarsController carsController,
                                         HealthController healthController,
                                         EmployeeController employeeController,
                                         TransformController transformController) {
            this.carsController = carsController;
            this.employeeController = employeeController;
            this.healthController = healthController;
            this.transformController = transformController;
        }

        @Override
        public void initialize(ConfigurableApplicationContext applicationContext) {
            applicationContext.getBeanFactory().registerSingleton("carsController", carsController);
            applicationContext.getBeanFactory().registerSingleton("employeeController", employeeController);
            applicationContext.getBeanFactory().registerSingleton("healthController", healthController);
            applicationContext.getBeanFactory().registerSingleton("transformController", transformController);
        }
    }
}

