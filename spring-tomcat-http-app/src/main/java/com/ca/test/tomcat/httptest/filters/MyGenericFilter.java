package com.ca.test.tomcat.httptest.filters;

import lombok.SneakyThrows;
import org.springframework.stereotype.Component;

import javax.servlet.FilterChain;
import javax.servlet.GenericFilter;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

@Component
public class MyGenericFilter extends GenericFilter {
    @Override
    @SneakyThrows
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) {
        HttpServletResponse resp = (HttpServletResponse) res;
        resp.addHeader("X-SERVLET", "Ololo trololo!");
        chain.doFilter(req, res);
    }
}
