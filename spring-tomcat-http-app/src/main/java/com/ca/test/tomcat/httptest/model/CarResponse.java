package com.ca.test.tomcat.httptest.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

public class CarResponse implements Serializable {
    @Getter
    @Setter
    private String id;

    @Getter
    @Setter
    private String status;
}
