package com.ca.test.tomcat.httptest.model;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@RequiredArgsConstructor
public class CarBkp implements Serializable {
    @Getter
    @Setter
    @NonNull
    private String id;
    @Getter
    @Setter

    private String plateNumber;

    @Getter
    @Setter
    @NonNull
    private String color;

    @Getter
    @Setter
    @NonNull
    private String model;
    @Getter
    @Setter
    private String chassisNumber;

    @Getter
    @Setter
    @NonNull
    private String productionYear;
    @Getter
    @Setter
    private String issueDate;
    @Getter
    @Setter
    private String expiryDate;

    @Getter
    @Setter
    private String status;
}
