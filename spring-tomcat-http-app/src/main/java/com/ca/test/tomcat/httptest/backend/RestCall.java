package com.ca.test.tomcat.httptest.backend;

import com.ca.test.tomcat.httptest.model.Car;
import com.google.gson.Gson;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class RestCall {
    private static final RestCall INSTANCE = new RestCall();

    public static RestCall getInstance() {
        return INSTANCE;
    }

    private RestTemplate restTemplate;

    private RestTemplate getRestTemplate() {
        if (restTemplate == null) {
            restTemplate = new RestTemplate();
        }
        return restTemplate;
    }


    public String callGetService(String uri, Map<String, String> reqHeaders) {
        RestTemplate restTemplate = getRestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        if(reqHeaders != null && !reqHeaders.isEmpty()) {
            Set<String> headerKeys = reqHeaders.keySet();
            for(String headerKey : headerKeys) {
                headers.add(headerKey, reqHeaders.get(headerKey));
            }
        }
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);

        System.out.println("GET Web Service Response: " + result);
        return result.getBody();
    }

    public String callPostService(String uri, Map<String, String> reqHeaders, String payload) {
        RestTemplate restTemplate = getRestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        if(reqHeaders != null && !reqHeaders.isEmpty()) {
            Set<String> headerKeys = reqHeaders.keySet();
            for(String headerKey : headerKeys) {
                headers.add(headerKey, reqHeaders.get(headerKey));
            }
        }

        HttpEntity<String> entity = new HttpEntity<String>(payload, headers);

        ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);

        System.out.println("POST Web Service Response: " + result);
        return result.getBody();
    }

    public String callRestService(String uri, Map<String, String> reqHeaders) {
        RestTemplate restTemplate = getRestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
        System.out.println("Web Service Response: " + result);
        return result.getBody();
    }

    public List<Car> getCars(String uri) {
        String json = callRestService(uri, null);
        Gson gson = new Gson();
        Car[] cars = gson.fromJson(json, Car[].class);
        return Arrays.asList(cars);
    }

    public Car getCar(String carJson) {
        Gson gson = new Gson();
        Car car = gson.fromJson(carJson, Car.class);
        return car;
    }
}
