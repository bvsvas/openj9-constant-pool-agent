package com.ca.test.tomcat.httptest.filters;

import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.prometheus.PrometheusConfig;
import io.micrometer.prometheus.PrometheusMeterRegistry;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.actuate.metrics.AutoTimer;
import org.springframework.boot.actuate.metrics.web.servlet.DefaultWebMvcTagsProvider;
import org.springframework.boot.actuate.metrics.web.servlet.WebMvcMetricsFilter;
import org.springframework.boot.actuate.metrics.web.servlet.WebMvcTagsProvider;
import org.springframework.stereotype.Component;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.function.Function;

@Component
public class MyFilterInstanceWrapper extends WebMvcMetricsFilter {
    private static final Logger logger = LoggerFactory.getLogger(MyOncePerRequestFilter.class);

    @Getter
    @Setter
    private int value = 2;

    @Getter
    @Setter
    private Function<String, Integer> stringToIntConverter = (String s) -> {
        try {
            return Integer.parseInt(s);
        } catch (NumberFormatException ex) {
            return null;
        }
    };

    public MyFilterInstanceWrapper(MeterRegistry registry, WebMvcTagsProvider tagsProvider, String metricName, AutoTimer autoTimer) {
        super(registry, tagsProvider, metricName, autoTimer);
        logger.info("MyFilterInstanceWrapper - WebMvcMetricsFilter - argument constructor");
    }

    public MyFilterInstanceWrapper() {
        this(new PrometheusMeterRegistry(PrometheusConfig.DEFAULT),
                new DefaultWebMvcTagsProvider(),
                "web.requests", AutoTimer.ENABLED);
        logger.info("MyFilterInstanceWrapper - WebMvcMetricsFilter - default constructor");
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        Integer result = getStringToIntConverter().apply("42");
        logger.info("MyFilterInstanceWrapper - WebMvcMetricsFilter originated by request {}, {}", request.getRequestURI(), result);

        super.doFilterInternal(request, response, filterChain);
    }
}
