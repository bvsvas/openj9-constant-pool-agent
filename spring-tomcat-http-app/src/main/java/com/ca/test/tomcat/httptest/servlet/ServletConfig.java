package com.ca.test.tomcat.httptest.servlet;

import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Configuration
@ServletComponentScan
public class ServletConfig {

    @Bean
    public ServletRegistrationBean<FrameworkServlet> helloServletRegistrationBean() {
        ServletRegistrationBean<FrameworkServlet> registrationBean = new ServletRegistrationBean<>(new FrameworkServlet(), "/hello");
        registrationBean.setLoadOnStartup(1);
        return registrationBean;
    }

    @Bean
    public ServletRegistrationBean<HttpServlet> helloInnerServletRegistrationBean() {
        ServletRegistrationBean<HttpServlet> registrationBean = new ServletRegistrationBean<>(new HttpServlet() {
            @Override
            protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
                resp.getWriter().println("Inner - Hello, World!");
            }
            @Override
            protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
                resp.getWriter().println("Inner - Hello, World!");
            }
        }, "/helloInner");
        registrationBean.setLoadOnStartup(1);
        return registrationBean;
    }

}
