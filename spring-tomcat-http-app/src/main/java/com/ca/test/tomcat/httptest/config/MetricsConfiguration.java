package com.ca.test.tomcat.httptest.config;

import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import io.micrometer.prometheus.PrometheusConfig;
import io.micrometer.prometheus.PrometheusMeterRegistry;
import org.springframework.boot.actuate.metrics.AutoTimer;
import org.springframework.boot.actuate.metrics.web.servlet.DefaultWebMvcTagsProvider;
import org.springframework.boot.actuate.metrics.web.servlet.WebMvcMetricsFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MetricsConfiguration {

    @Bean
    public MeterRegistry meterRegistry() {
        return new PrometheusMeterRegistry(PrometheusConfig.DEFAULT);
    }

    @Bean
    public FilterRegistrationBean<WebMvcMetricsFilter> webMetricsFilter(MeterRegistry meterRegistry) {
        FilterRegistrationBean<WebMvcMetricsFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new WebMvcMetricsFilter(meterRegistry,
                new DefaultWebMvcTagsProvider(),
                "web.requests", AutoTimer.ENABLED));
        registrationBean.addUrlPatterns("/*");
        return registrationBean;
    }
}

