package com.ca.test.tomcat.httptest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringEmployeeApp {

	public static void main(String[] args) {
		System.setProperty("tomcat.util.http.parser.HttpParser.requestTargetAllow","|{}:");
		SpringApplication.run(SpringEmployeeApp.class, args);
	}
}
